function each(elements, cb) {
    if(elements instanceof Array){
        for(var i = 0;i<elements.length;i++){
            cb(elements[i],i)
        }
    }
    else{
        console.log("Not an array")
    }
    
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
}

module.exports = each;