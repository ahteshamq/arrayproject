function filter(elements, cb) {
    if(elements instanceof Array){
        const arr =[];
        let flag = false
        let arrayLength = elements.length
        for(let index = 0;index<arrayLength;index++){
            if(cb(elements[index],index,elements)===true){
                arr.push(elements[index])
                flag = true;
            }
        }
        if(flag){
            return arr
        }
        else{
            return[]
        }
    }
    else{
        console.log("Not an array.")
    }
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
}

module.exports = filter;
