function flatten(elements,depth) {
    if(elements instanceof Array){
        if(depth === undefined){
            depth = 1
        }
        let res = []
        function getElement(arr,depth){
            let deep = depth
            for(let index = 0;index<arr.length;index++){
                if(arr[index] instanceof Array && deep!=0){
                    deep-=1
                    getElement(arr[index],deep)
                }else if(arr[index]=== undefined){
                    //pass
                }
                else{
                    res.push(arr[index])
                }
                deep = depth
            }
            
        }
        getElement(elements,depth)
        return res;
        
    }
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
}
 module.exports = flatten;